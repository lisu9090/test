#middle
middleCell = (np.floor(numth/2), np.ceil((dim/2)%thdim), np.ceil(dim/2))

if(rank == middleCell[0]):
	threadData[middleCell[1], middleCell[2]] = v

#wait for all	
comm.Barrier()

#temporary data store
tmp = np.zeros([thdim, dim])

for kx in range(k-1):
	#communication
	topData = comm.recive()
	bottomData = comm.recive()
	
	#add recived data to thread data to simplify computation in loops
	if(rank != 0):
		np.concatenate(threadData, topData)
	if(rank != numth-1):
		np.concatenate(threadData, bottomData)
	
	#compute new values and save it in tmp	
	for x in range(1, threadData.shape[0] - 2):
		for y in range(1, threadData.shape[1]-2):
			tmp[x,y] = (threadData[x-1,y] + threadData[x+1,y] + threadData[x,y-1] + threadData[x,y+1])/4			
	
	#if you are 0 set middle cell to v
	if(rank == middleCell[0]):
		tmp[middleCell[1], middleCell[2]] = v
	
	#update thread data matrix
	threadData = np.copy(tmp)

#prepare recive buffer	
recvbuf = None
if rank == 0:
    recvbuf = np.empty([size*thdim, dim], dtype='i')

#collect data from all threads in 0
comm.Gather(threadData, recvbuf, root=0)

#concatenate threads data to output matrix
if(rank == 0):
	outData = np.concatenate(recvbuf, threadData)