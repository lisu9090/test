#!/usr/bin/env python
from mpi4py import MPI
import numpy as np
import sys

np.set_printoptions(threshold=np.nan)

#get time
time = MPI.Wtime();

#initialize info about rank and nuber of threads
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
numth = comm.Get_size()

#set default task dimension, nmber of steps, electric potenpial
dim = 10.0
k = 50
v = 10.0

#sys.argv: 0 - program name, 1 - dim, 2 - number of steps, 3 - value of electric potenpial
#get cstom task dimension
if(len(sys.argv) > 1 and float(sys.argv[1]) > 1):
        dim = float(sys.argv[1])
if(len(sys.argv) > 2 and int(sys.argv[1]) > 0):
        k = int(sys.argv[2])
if(len(sys.argv) > 3):
        v = float(sys.argv[3])

#calculate thread data size
threadDim = np.ceil(dim/numth)

#arrange thread data matrix
threadData = np.random.random([threadDim, dim]) * v

#if rank 0 set first row to 0, eles if last rank set last row to 0
if(rank == 0):
        threadData[0,:] = np.zeros([dim], dtype='f')
elif(rank == numth - 1):
        threadData[threadData.shape[0] -1, :] = np.zeros([dim], dtype='f')

#all threads set first and last column to 0
threadData[:,0] = np.zeros([threadDim], dtype='f')
threadData[:, threadData.shape[1] -1] = np.zeros([threadDim], dtype='f')

#find middle and set it to v
middleCell = (np.floor(numth/2), np.floor(((threadDim*numth)/2)%threadDim), np.ceil(dim/2)) #(rank, x, y)

if(rank == middleCell[0]):
        threadData[middleCell[1], middleCell[2]] = v
        if(numth > 1):
                middleCell = (middleCell[0], middleCell[1] + 1, middleCell[2])

#wait for all
comm.Barrier()

#temporary data store
tmp = np.zeros([threadDim, dim], dtype='f')

for kx in range(k-1):
        if(kx%10 == 0 and rank == 0):
                print 'Epoch #', kx

        if(numth > 1):
                #buffers
                topData = np.zeros([1, dim])
                bottomData = np.zeros([1, dim])

                #communication down
                if(rank == 0):
                        comm.Send([threadData[threadDim-1, :], MPI.DOUBLE], dest = rank+1)
                elif(rank != numth-1):
                        if(rank % 2 == 0):
                                comm.Send([threadData[threadDim-1, :], MPI.DOUBLE], dest = rank+1)
                                comm.Recv([topData, MPI.DOUBLE], source = rank-1)
                        else:
                                comm.Recv([topData, MPI.DOUBLE], source = rank-1)
                                comm.Send([threadData[threadDim-1, :], MPI.DOUBLE], dest = rank+1)
                else:
                        comm.Recv([topData, MPI.DOUBLE], source = rank-1)

                #communication up
                if(rank == numth-1):
                        comm.Send([threadData[0, :], MPI.DOUBLE], dest = rank-1)
                elif(rank != 0):
                        if(rank % 2 == 0):
                                comm.Send([threadData[0, :], MPI.DOUBLE], dest = rank-1)
                                comm.Recv([bottomData, MPI.DOUBLE], source = rank+1)
                        else:
                                comm.Recv([bottomData, MPI.DOUBLE], source = rank+1)
                                comm.Send([threadData[0, :], MPI.DOUBLE], dest = rank-1)
                else:
                        comm.Recv([bottomData, MPI.DOUBLE], source = rank+1)

                #add recived data to thread data to simplify computation in loops
                if(rank != 0):
                                threadData = np.concatenate((topData, threadData), axis = 0)
                if(rank != numth-1):
                                threadData = np.concatenate((threadData, bottomData), axis = 0)

        #compute new values and save it in tmp
        for x in range(1, threadData.shape[0] - 1):
                for y in range(1, threadData.shape[1]-1):
                        if(rank == 0):
                                tmp[x,y] = (threadData.item((x-1,y)) + threadData.item((x+1,y)) + threadData.item((x,y-1)) + threadData.item((x,y+1)))/4
                        else:
                                tmp[x-1,y] = (threadData.item((x-1,y)) + threadData.item((x+1,y)) + threadData.item((x,y-1)) + threadData.item((x,y+1)))/4

        #set middle value to v
        if(rank == middleCell[0]):
                tmp[middleCell[1], middleCell[2]] = v

        #update thread data matrix
        threadData = np.copy(tmp)

#prepare recive buffer
recvbuf = None
if rank == 0:
    recvbuf = np.empty([numth*threadDim, dim], dtype='d')

#collect data from all threads in 0
comm.Gather(threadData, recvbuf, root=0)

#concatenate threads data to output matrix, print time
if(rank == 0):
	np.savetxt('output.txt', recvbuf, delimiter=',')
	print 'Time: ', MPI.Wtime() - time, 's'
